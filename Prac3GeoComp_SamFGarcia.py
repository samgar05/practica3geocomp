'''
Samuel Fernando Garcia Herrera 
Prac3. Sistemas dinamicos y Teorema de Liouville
'''

import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull, convex_hull_plot_2d
from matplotlib.animation import FuncAnimation


#Partiendo del hamiltoniano H(q,p) = p^2 + (1/4)*(q^2 -1)^2
#Ecuación del sistema dinámico
def F(q) -> np.array:
    ddq = -2*q*(q**2 - 1)
    return ddq

def derivada(q,dq0,d) -> np.array:
   #dq = np.empty([len(q)])
   dq = (q[1:len(q)]-q[0:(len(q)-1)])/d
   dq = np.insert(dq,0,dq0) #dq = np.concatenate(([dq0],dq))
   return dq

def orbita(n,q0,dq0,F, args=None, d=0.001) -> np.array:

    q = np.empty([n+1])
    q[0] = q0
    q[1] = q0 + dq0*d
    for i in np.arange(2,n+1):
        args = q[i-2]
        q[i] = - q[i-2] + d**2*F(args) + 2*q[i-1]
    return q 
 
#################################################################    
#  ESPACIO FÁSICO
################################################################# 
## Pintamos el espacio de fases

d = 10**(-4)

def simplectica(q0,dq0,F,color=0,d = 10**(-4),n = int(20/d),marker='-'): 
    q = orbita(n,q0=q0,dq0=dq0,F=F,d=d)
    dq = derivada(q,dq0=dq0,d=d)
    p = dq/2
    plt.plot(q, p, marker,c=plt.get_cmap("bone")(color))


horizonte_temporal = 12
d = 10**(-3)

figura = plt.figure(figsize=(8,5))
figura.subplots_adjust(hspace=0.4, wspace=0.2)
ejes = figura.add_subplot(1,1, 1)

#Condiciones iniciales:
vector_q0 = np.linspace(0.,1.,num=20) #Consideramos 20 puntos iniciales y sus respectivas orbitas
vector_dq0 = np.linspace(0.,2,num=20)

for i in range(len(vector_q0)):
    for j in range(len(vector_dq0)):
        q0 = vector_q0[i]
        dq0 = vector_dq0[j]
        color = (1+i+j*(len(vector_q0)))/(len(vector_q0)*len(vector_dq0))

        simplectica(q0=q0,dq0=dq0,F=F,color=color,marker = 'ro', d= 10**(-3),n = int(horizonte_temporal/d))
        
ejes.set_xlabel("q(t)", fontsize=12)
ejes.set_ylabel("p(t)", fontsize=12)

plt.show()


#################################################################    
# VARIACION DEL AREA DE D_(0,t)
################################################################# 

vector_horizontes = np.linspace(0.01, 1/3, num= 10) # en este caso num es el numero de analisis que hacemos 
vector_area = np.array([])

for horizonte in vector_horizontes: #notese que hacemos la simulacion de cero para cada horizonte t

    vector_q0 = np.linspace(0.0,1.0,num=20)
    vector_dq0 = np.linspace(0.0,2,num=20)
    vector_q = np.array([])
    vector_p = np.array([])
    for i in range(len(vector_q0)):
        for j in range(len(vector_dq0)):
            q0 = vector_q0[i]
            dq0 = vector_dq0[j]
            d = 10**(-3)
            n = int(horizonte/d)


            q = orbita(n,q0=q0,dq0=dq0,F=F,d=d)
            dq = derivada(q,dq0=dq0,d=d)
            p = dq/2

            vector_q = np.append(vector_q,q[-1])
            vector_p = np.append(vector_p,p[-1])
    

    X = np.array([vector_q,vector_p]).T
    hull = ConvexHull(X)
    

    convex_hull_plot_2d(hull) #descomentando esto podemos graficar convexhull
    
    area = hull.volume
    
    vector_area = np.append(vector_area, area)


plt.plot(vector_horizontes, vector_area)
plt.title("Analisis del área")
plt.xlabel("Valor del parámetro t")
plt.ylabel("Area de la región")
plt.show()


########################################################
#SENSIBILIDAD DEL AREA POR LOS DELTAS
########################################################

import time

t1 = time.perf_counter()
vector_deltas = np.linspace(10**(-4), 10**(-3), num=50) #con num = k tarda al rededor de k segundos
vector_area = np.array([])
horizonte = 1/3
for delta in vector_deltas: #notese que hacemos la simulacion de cero para cada delta

    vector_q0 = np.linspace(0.0,1.0,num=20)
    vector_dq0 = np.linspace(0.0,2,num=20)
    vector_q = np.array([])
    vector_p = np.array([])

    for i in range(len(vector_q0)):
        for j in range(len(vector_dq0)):
            q0 = vector_q0[i]
            dq0 = vector_dq0[j]

            d = delta
            n = int(horizonte/d)

            q = orbita(n,q0=q0,dq0=dq0,F=F,d=d)
            dq = derivada(q,dq0=dq0,d=d)

            p = dq/2
            vector_q = np.append(vector_q,q[-1])
            vector_p = np.append(vector_p,p[-1])
            

    X = np.array([vector_q,vector_p]).T
    hull = ConvexHull(X)
    

    #convex_hull_plot_2d(hull) #graficar convexhull
    
    area = hull.volume
    vector_area = np.append(vector_area, area)

plt.close()
plt.plot(vector_deltas, vector_area)
plt.title("Analisis del área")
plt.xlabel("Valor del parámetro $\delta$ con horizonte = 1/3")
plt.ylabel("Area de la región")
plt.show()

t2 = time.perf_counter()

print(t2-t1)


########################################################
#####GIF CADA FRAME SE HACE CON
########################################################
# Crear una función para generar las gráficas

# Crear una figura fuera de la función animate
figura, ejes = plt.subplots(figsize=(5, 5))

# Crear una función para generar las gráficas
def animar(horizonte):
    ejes.clear()  # Limpiar el eje en cada iteración
    vector_q0 = np.linspace(0.0, 1.0, num=20)
    vector_dq0 = np.linspace(0.0, 2, num=20)
    for q0 in vector_q0:
        for dq0 in vector_dq0:
            d = 10**(-3)
            n = int(horizonte / d)
            q = orbita(n, q0=q0, dq0=dq0, F=F, d=d)
            dq = derivada(q, dq0=dq0, d=d)
            p = dq / 2
            ejes.set_xlim(-2.2, 2.2)
            ejes.set_ylim(-1.2, 1.2)
            ejes.set_xlabel("q(t)", fontsize=12)
            ejes.set_ylabel("p(t)", fontsize=12)
            ejes.plot(q[-1], p[-1], marker="o", markersize=5, markeredgecolor="blue", markerfacecolor="blue")
    return figura,

# Creamos la animacion a partir de los fotogramas
vector_horizontes = np.linspace(0.01, 5, num=10)
ani = FuncAnimation(figura, animar, frames=vector_horizontes, interval=200)

# guardamos el gif
ani.save('animacion2.gif', writer='pillow', fps=4)
plt.show()



