'''
Samuel Fernando Garcia Herrera 
Prac3. 
'''

import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull, convex_hull_plot_2d


import warnings
warnings.filterwarnings("ignore")

#Partiendo del hamiltoniano H(q,p) = p^2 + (1/4)*(q^2 -1)^2

def F(q : np.array) -> np.array:
    ddq = -2*q*(q**2 - 1)
    return ddq

def derivada(q:np.array, dq0:float, delta:float) -> np.array:
   dq = (q[1:len(q)]-q[0:(len(q)-1)])/delta
   dq = np.insert(dq,0,dq0)
   return dq

def orbita(q0, dq0, F, delta, args = None) -> np.array: #nos da un array con los valores de la orbita de q 
    
    n = int(16/delta)
    
    q = np.empty([n+1])
    q[0] = q0
    q[1] = q0 + dq0*delta
    for i in np.arange(2,n+1):
        args = q[i-2]
        q[i] = - q[i-2] + delta**2*F(args) + 2*q[i-1]
    return q #np.array(q),

def periodos(q,d,max=True):
    #Si max = True, tomamos las ondas a partir de los máximos/picos
    #Si max == False, tomamos las ondas a partir de los mínimos/valles
    epsilon = 5*d
    dq = derivada(q,dq0=None,d=d) #La primera derivada es irrelevante
    if max == True:
        waves = np.where((np.round(dq,int(-np.log10(epsilon))) == 0) & (q >0))
    if max != True:
        waves = np.where((np.round(dq,int(-np.log10(epsilon))) == 0) & (q <0))
    diff_waves = np.diff(waves)
    waves = waves[0][1:][diff_waves[0]>1]
    pers = diff_waves[diff_waves>1]*d
    return pers, waves

# espacio de fases

## Pintamos el espacio de fases
def simplectica(q0:float,dq0:float,F,delta:float, indice_color= 0, marker='-') -> None: 
    
    q = orbita(q0,dq0,F,delta)
    dq = derivada(q,dq0,delta)
    p = dq/2
    plt.plot(q, p, marker= 'o',c=plt.get_cmap("bone")(indice_color))

#frames del gif
'''
def frame(horizonte_temporal:float) -> (np.array, np.array):
    
    vector_q0 = np.linspace(0.,1.,num=14)
    vector_dq0 = np.linspace(0.,2,num=14)
    
    q2 = np.array([])
    p2 = np.array([])
    
    for i in range(len(vector_q0)):
        for j in range(len(vector_dq0)):
            q0 = vector_q0[i]
            dq0 = vector_dq0[j]
            
            delta = 10**(-3)
            n = int(horizonte_temporal/delta)
            t = np.arange(n+1)*d
            
            q = orbita(q0, dq0, F, delta)
            dq = derivada(q, dq0, delta)
            p = dq/2
            
            q2 = np.append(q2,q[-1])
            p2 = np.append(p2,p[-1])
    return q2, p2


#~fff
def frame(horizonte_temporal:float, delta) -> (np.array, np.array):
    vector_q0 = np.linspace(0.,1.,num=14)
    vector_dq0 = np.linspace(0.,2,num=14)
    
    vector_q = np.array([])
    vector_p = np.array([])
    
    for i in range(len(vector_q0)):
        for j in range(len(vector_dq0)):
            q0 = vector_q0[i]
            dq0 = vector_dq0[j]
            
            
            q = orbita(q0, dq0, F, delta)
            dq = derivada(q, dq0, delta)
            p = dq/2
            
            vector_q = np.append(vector_q,q[-1])
            vector_p = np.append(vector_p,p[-1])
    return vector_q, vector_p

plt.rcParams["legend.markerscale"] = 6 
q, p = frame(0.001, 0.001)


ejes = fig.add_subplot(1,1, 1)
plt.xlim(-2.2, 2.2)
plt.ylim(-1.2, 1.2)

    ejes.set_xlabel("q(t)", fontsize=12)
    ejes.set_ylabel("p(t)", fontsize=12)
    plt.plot(q[-1], p[-1], marker="o", markersize= 5, markeredgecolor="blue",markerfacecolor="blue")
plot.show
'''

if __name__ == "__main__":
    #Parte1. Representacion grafica del espacio fasico
    delta = 0.001

    fig = plt.figure(figsize=(5,5))
    fig.subplots_adjust(hspace=0.4, wspace=0.2)
    ejes = fig.add_subplot(1,1, 1)
    
    #Condiciones iniciales:
    vector_q0 = np.linspace(0.0, 1.0, num=14) #num es el numero de valores iniciales entre 0 y 1
    vector_dq0 = np.linspace(0.0, 2, num=14) 
    
    for i in range(len(vector_q0)):
        for j in range(len(vector_dq0)):
            q0 = vector_q0[i]
            dq0 = vector_dq0[j]
            indice_color = (1+i+j*(len(vector_q0)))/(len(vector_q0)*len(vector_dq0))
            #ax = fig.add_subplot(len(seq_q0), len(seq_dq0), 1+i+j*(len(seq_q0)))
            simplectica(q0 ,dq0,F,delta, indice_color, marker='ro')
            
    ejes.set_xlabel("q(t)", fontsize=15)
    ejes.set_ylabel("p(t)", fontsize=15)
    #fig.savefig('Simplectic.png', dpi=250)
    plt.show()

    #Parte2. Areas de las orbitas
    #cubrimos cada orbita con una cobertura convexa y sacamos su area
    
    horizonte_temporal = 1/3


    #Parte3. Creamos GIF 
    
    
    
    
    
    
    